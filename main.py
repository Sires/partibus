#This game was created with Tkinter version 8.6 and Python version 3.5.2
from tkinter import *
import time
import os
import pars_api
import sys
import json

Width  = 500
Height = 500

path = os.getcwd()
home_path = os.path.expanduser("~")
image_list = {"title": path + "/game/images/Title.png", "create_world_button": path + "/game/images/CreateWorld.png", "choose_world_button": path + "/game/images/ChooseWorld.png"}

def is_in_coords(x : int, y: int, x_min: int, y_min: int, x_max: int, y_max: int):
	if x <= x_max and x >= x_min and y <= y_max and y >= y_min:
		return True
	else:
		return False

def dofile(path):
	exec(open(path).read())

def on_click(event):
	xy = app.get_pos_canvas("img.CreateWorld")
	#In the x_min param there is xy["x"] - 200/2(and something similar with y_min) because the x and y of the object is the center of the object, so I need to subtract(and add in the x_max) size/2
	if is_in_coords(event.x, event.y, xy["x"] - 200/2, xy["y"] - 30/2, xy["x"] + 200/2, xy["y"] + 30/2):
		app.delete_item_canvas("all")
		close = True
		main_game()

class ImageBuffer():
	def __init__(self):
		self.buffer = {}

	def add_image(self, key, path):
		self.buffer[key] = PhotoImage(file = path)

	def get_image(self, key):
		return self.buffer[key]

class Window(Frame):
	def __init__(self, master = None):
		Frame.__init__(self, master)
		self.addx = 0
		self.master = master
		self.canvas_itens = {}
		self.initall()

	def initall(self):
		self.master.title("Partibus")
		self.pack(fill = BOTH, expand = YES)
		self.canvas = Canvas(self, width = Width, height = Height, bg = "#444444")
		self.canvas.bind("<Configure>", self.on_resize)
		self.canvas.pack(fill = BOTH, expand = YES)

	def on_resize(self, event):
		self.addx = round(event.width / 2)
		for item in self.canvas_itens:
			if self.canvas_itens[item][3].get("centerx", False) != False:
				if self.canvas_itens[item][3].get("basex", False) != False:
					self.set_pos_canvas(item, self.addx + self.canvas_itens[item][3]["basex"], self.canvas_itens[item][2])
				else:
					self.set_pos_canvas(item, self.addx, self.canvas_itens[item][2])

	def add_text_canvas(self, key: str, text: str, size: int, x: int, y: int, options: dict = {}):
		self.canvas_itens["txt." + key] = [self.canvas.create_text(x, y, text = text), x, y, options]

	def add_image_canvas(self, key: str, img: PhotoImage, x: int, y: int, options: dict = {}):
		self.canvas_itens["img." + key] = [self.canvas.create_image(x, y, image = img), x, y, options]

	def set_pos_canvas(self, key: str, x: int, y: int):
		self.canvas.coords(self.canvas_itens[key][0], x, y)
		self.canvas_itens[key][1] = x
		self.canvas_itens[key][2] = y

	def get_pos_canvas(self, key: str):
		return {"x": self.canvas_itens[key][1], "y": self.canvas_itens[key][2]}

	def delete_item_canvas(self, key: str):
		if key == "all":
			self.canvas.delete("all")
		else:
			self.canvas.delete(self.canvas_itens[key])

root = Tk()

ScreenWidth = round((root.winfo_screenwidth() / 2) - (Width / 2))
ScreenHeight = round((root.winfo_screenheight() / 2) - (Height / 2))

root.geometry("500x500+" + str(ScreenWidth) + "+" + str(ScreenHeight))
root.minsize(500, 500)

root.bind("<Button-1>", on_click)

app = Window(root)

ib = ImageBuffer()

for k in image_list:
	ib.add_image(k, image_list[k])

stage = None
close = False

def main_window():
	stage = "mw"
	app.add_image_canvas("Title", ib.get_image("title"), 0, 120, {"centerx": True})
	app.add_image_canvas("CreateWorld", ib.get_image("create_world_button"), 0, 220, {"centerx": True, "basex": -100})
	#app.add_image_canvas("ChooseWorld", ib.get_image("choose_world_button"), 0, 260, {"centerx": True, "basex": -100})
	#This will be used when the choose world option be completed
	while close == False:
		root.update()
		time.sleep(0.1)

def main_game():
	partibus = {}
	entire_map = {}
	map_sector = {}
	player = {}
	#DPE = Dot Partibus Exists
	dpe = os.path.exists(home_path + "/.partibus")
	if dpe == False:
		try:
			os.chdir(home_path)
			os.mkdir(".partibus")
			os.chdir(home_path + "/.partibus")
			os.mkdir("worlds")
			os.chdir(home_path + "/.partibus/worlds")
			os.mkdir("world")
			os.chdir(home_path + "/.partibus/worlds/world")
			open("map.json", "w").close()
			open("players.json", "w").close()
		except:
			print("Failed into creating .partibus folder in home dir or creating the map/player files, error: \n\n", e.__doc__, "\n\n Please try to delete the .partibus folder if it was created.")
	else:
		try:
			os.chdir(home_path + "/.partibus/worlds/world")
			file = open("map.json", "r")
			if file.read() != "":
				entire_map = json.loads(file.read())
				file.close()
			file = open("players.json", "r")
			if file.read() != "":
				players = json.loads(file.read())
				file.close()
		except Exception as e:
			print("Failed into opening map/players files, error: \n\n", e.__doc__)

main_window()